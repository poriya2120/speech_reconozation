import speech_recognition as sr
import time
import playsound
import os
import random
from gtts import gTTs
from time import ctime
import webbrowser
r=sr.Recognizer()
def record(ask=False):
    with sr.Microphone() as source:
        
        audio=r.listen(source)
        if ask:
            speak(ask)
        # voice_data=r.recognize_google(audio)
        # print(voice_data)
        voice_data=''
        try:
            voice_data=r.recognize_google(audio)
            # print(voice_data)
        except sr.UnKnownValueError:
            speak('sorray, I did not understand')
        except sr.RequestError:
            speak('sorray, speech is down')
        return voice_data

def speak(audio_string):
    tts=gTTs(Text=audio_string,lang='en')
    r=random.randint(1,10000000)
    audio_file='audio'+str(r)+'.mp3'
    tts.save(audio_file)
    playsound.playsound(audio_file)
    speak(audio_string)
    #if remove mp3 files
    os.remove(audio_file)

def respond(voice_data):
    if 'what is your name' in voice_data:
        speak('My name is poriya')
    if 'what time is it' in voice_data:
        speak(ctime())
    if 'search' in voice_data:
        search=record('what do you want search?')
        url='https://google.com/search?q='+search
        webbrowser.get().open(url)
        speak('result search is'+search)
    if 'location' in voice_data:
        location=record('what is location?')
        url='https://google.nl/maps/place/'+location+'/&amp;'

        webbrowser.get().open(url)
        speak('location  is'+location)
    if 'bye' in voice_data:
        exit()


time.sleep(1)
speak('How can I help you?')
while 1:
    voice_data=record()
    respond(voice_data)

